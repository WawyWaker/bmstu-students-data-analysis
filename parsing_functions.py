from bs4 import BeautifulSoup
import pandas as pd
import time
from selenium import webdriver
import numpy as np


# todo: automate the parsing stage (chrome must be opened). currently all functions open it separately.

def get_session_links():
    # 24.07.2021: this function opens eu and gets all links of all sessions.
    # Output: currently outputs all links in a file.

    # todo: output pandas DataFrame

    # opening eu.bmstu.ru in chrome
    browser = webdriver.Chrome()
    browser.get("https://eu.bmstu.ru/")

    # opening sessions page
    time.sleep(7)
    browser.get("https://eu.bmstu.ru/modules/session/")

    # getting https://eu.bmstu.ru/modules/session/ page
    source_data = browser.page_source

    # initializing BS
    soup = BeautifulSoup(source_data, "lxml")

    # looking for all li's in ul with "term-select" id
    session_links = soup.find("body").find("ul", id="term-select").find_all("li")

    # getting all links that are located in "href" of a and making a pandas DF with names of sessions and links
    links = []
    for link in session_links:
        links.append([str("https://eu.bmstu.ru/" + link.find("a").get("href")), link.find("a").text])

    # writing results to the file
    f = open("sesson_links.txt", "w")
    for l, name in links:
        f.write(l + "\n")  # + "\t" + name + "\n")


def parse_students(group_links, list_of_faculties):
    # 24.07.2021: this function opens every session page and gets links of some group. after it opens every page
    # where marks of all students are presented. after it makes a pandas DataFrame with data of a group and
    # appends it to the "groups". when all groups are parsed it makes a DataFrame with all groups, students,
    # marks and subjects

    # outputs: DataFrame where columns: subjects; rows - students and their marks.

    # opening chrome and eu.bmstu.ru for logging in

    browser = webdriver.Chrome()
    browser.get("https://eu.bmstu.ru")

    #time.sleep(10)

    #path = "E:\\GitLab\\bmstu-students-data-analysis\\data\\"
    path = "C:\\Users\\Wawe\\PycharmProjects\\bmstu-students-data-analysis\\data\\"
    for group_name in group_links.names.unique().tolist():

        if any(group_name.split("-")[0][:2].lower() in faculty for faculty in list_of_faculties):
            if (len(group_name.split("-")[1]) == 3 and group_name.split("-")[1][-1] == "Б" and int(
                    group_name.split("-")[1][:-1]) < 70) or (
                    len(group_name.split("-")[1]) == 2 and int(group_name.split("-")[1]) < 70):

                groups = []

                for link in group_links.links.loc[group_links["names"] == group_name]:
                    browser.get("https://eu.bmstu.ru/modules/session/?session_id=" + str(int(
                        group_links.session_id.loc[group_links.links == link].values)))
                    browser.get("https://eu.bmstu.ru" + link)
                    source_data = browser.page_source

                    soup = BeautifulSoup(source_data, "lxml")
                    new_group = get_students(soup)

                    if new_group.empty is not True:
                        normal_new_group = new_group.loc[:, new_group.columns != "ФИО"].applymap(lambda value: 5
                        if value.lower() == "отл" else (4 if value.lower() == "хор" else (3 if value.lower() ==
                                                                                               "удов" else (
                            np.nan if value.lower() == "зчт" else 2))))

                        normal_new_group["session_id"] = int(
                            group_links.session_id.loc[group_links.links == link].values)

                        normal_new_group["ФИО"] = new_group["ФИО"]

                        groups.append(normal_new_group)

                if groups:
                    groups_dataframe = pd.concat(groups, ignore_index=True)

                groups_dataframe.to_csv(path + group_name + ".txt", encoding="UTF-8", sep="\t")


def get_students(soup):
    # lloking for table where all links on pages with group's info are located
    table = soup.find("table")

    # if table is not empty starts the loop
    if table:
        table_head_column_list = []
        table_body_column_list = []
        table_body_column_list_buff = []

        for table_head_column in table.select("th"):
            if table_head_column.find("span") is not None:
                # print(table_head_column.find("span").text)
                table_head_column_list.append(table_head_column.find("span").text)
            else:
                # print(table_head_column.text)
                table_head_column_list.append(table_head_column.text)

        table_body = table.find("tbody")

        for table_body_row in table_body.select("tr"):
            for table_body_column in table_body_row.select("td"):

                if table_body_column.find("span") is not None:
                    # print(table_body_column.span.text)
                    table_body_column_list_buff.append(table_body_column.span.text)
                elif table_body_column.find("span") is None:
                    # print(table_body_column.text[2:])
                    table_body_column_list_buff.append(table_body_column.text[2:])

            table_body_column_list.append(table_body_column_list_buff.copy())
            table_body_column_list_buff = []

        data_set = [table_head_column_list]

        for row in table_body_column_list:
            data_set.append(row)

        return_dataframe = pd.DataFrame(data_set[1:], columns=data_set[0])
        del return_dataframe["№"]

        # for analysis only subjects with marks (2, 3, 4, 5) are suitable. there is no sense to analyse "зачет/не зачет"
        # these subjects for years 1 to 3 have marks:
        subjects = ["фио", "материаловедение", "иностранный", "термодинамика",
                    "детали машин", "аналитическая геометрия",
                    "математический анализ", "информатика", "начертательная геометрия", "инженерная графика",
                    "кратные интегралы и ряды", "теоретическая механика", "сопротивление материалов", "физика",
                    "метрология", "технология конструкционных материалов", "теория механизмов и машин",
                    "детали машин",
                    "интегралы и дифференциальные уравнения", "химия", "гидравлика", "Кратные интегралы и ряды",
                    "электротехника и электроника", "Инженерная и компьютерная графика"]

        # making DataFrame of columns with subjects with marks
        cols = [col for col in return_dataframe.columns if any(subj == col.lower() for subj in subjects)]

        if len(cols) == 0:
            return pd.DataFrame()
        else:
            return return_dataframe[cols]


    else:
        return pd.DataFrame()


def get_groups_and_links_dataframe(session_type):
    browser = webdriver.Chrome()
    browser.get("https://eu.bmstu.ru/")
    time.sleep(10)

    links = []
    names = []
    session_id = []

    for i in session_type:
        session_link = "https://eu.bmstu.ru/modules/session/?session_id=" + str(i)
        browser.get(session_link)

        # initializing BS
        soup = BeautifulSoup(browser.page_source, "lxml")

        # looking for common ul for all links in the table
        ul_shortcuts = soup.find("ul", class_="ul-struct").find_all("a")

        # getting links and names of groups
        for shortcut in ul_shortcuts:
            links.append(shortcut.get("href"))
            names.append(shortcut.text)
            session_id.append(i)

        # creating a pandas DF and choosing all students from "АК1-21" department

    group_links = pd.concat(
        [pd.Series(names, name="names"), pd.Series(links, name="links"), pd.Series(session_id, name="session_id")],
        axis=1)

    group_links.to_csv("winter_session_time.txt", encoding="UTF-8")

    return group_links
