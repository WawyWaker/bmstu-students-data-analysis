from parsing_functions import *


pd.set_option("display.max_columns", None, "display.max_rows", None)

summer_session_time = range(7, 32, 2)
winter_session_time = range(6, 32, 2)


# todo: make a function that will find similar subject names

# this function merges columns with same subjects that were renamed after the year 2012
def merge_old_and_new_subjects(dataFrame):

    output_dataframe = pd.DataFrame()
    old_new_names = [
        [
            "Линейная алгебра и функции нескольких переменных", "Линейная алгебра и функции многих переменных"
        ],
        [
            "Обыкновенные диференциальные уравнения", "Интегралы и дифференциальные уравнения"
        ],
        [
            "Физическая культура", "Элективный курс по физической культуре и спорту"
        ]
    ]
    for subject in old_new_names:
        dataFrame[subject[1]] = dataFrame[subject[1]].fillna(dataFrame[subject[0]])

    for columnName, columnData in dataFrame.iteritems():
        if columnData.isna().sum() == 0:
            output_dataframe[columnName] = columnData

    return output_dataframe


parse_students(pd.read_csv("winter_group_links.txt", encoding="UTF-8", index_col=0, sep="\t"), ["см"])

